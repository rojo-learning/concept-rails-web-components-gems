# Rails + Polymer Web Components con gemas

Éste es un pequeño proyecto de Rails para mostrar la inclusión de componentes web dentro de una aplicación.

El repositorio fue creado como muestra para el tema tratado en [«Uso de web components con Rails 2.5.x»][blog].

- [Rails + Polymer Web Components con gemas](#rails-polymer-web-components-con-gemas)
  - [Uso de la aplicación](#uso-de-la-aplicacion)
    - [Instalación de gemas y modulos](#instalacion-de-gemas-y-modulos)
    - [Entorno virtual con Vagrant](#entorno-virtual-con-vagrant)
      - [Comandos útiles para el entorno virtual](#comandos-utiles-para-el-entorno-virtual)

## Uso de la aplicación

- Antes que nada, clona el repositorio:

  `git clone git clone https://akaiiro@bitbucket.org/akaiiro_projects/concept-rails-web-components-gems.git rails-polymer`

Se asume que el equipo que corre la aplicación ya tiene instalado el siguiente stack:
- Ruby 2.5.x
- Node.js estable

**Nota**: Si no deseas instalar todas las dependencias sobre tu equipo de desarrollo, recomiendo que uses la máquina virtual definida con éste proyecto. Para ello, realiza primero los pasos definidos en la sección [«Entorno virtual con Vagrant»](#entorno-virtual-con-vagrant).

### Instalación de gemas y modulos

- Instala las dependencias del proyecto:

  ```bash
  # muevete a la carpeta del proyecto
  cd rails-polymer

  # instala las gemas del proyecto
  bundle install
  ```

- Inicia el servidor:

  `bundle exec rails serve`

- Abre el navegador en la página del servidor [http://localhost:3000][localhost].

### Entorno virtual con Vagrant

El entorno virtual instala todas las dependencias dentro de una maquina virtual para no contaminar tu equipo de desarrollo con software que tal vez sólo utilices para éste proyecto. Así, cuando termines de jugar con el proyecto, simplemente puedes destruir la maquina virtual y dejar sólo el código para poder usarlo de nuevo en el futuro.

Para poder usar éste y otros entornos virtuales, necesitas instalar:

- [VirtualBox][vbox] (y su paquete de extensiones).
- [Vagrant][vagrant].

Una vez que tengas VirtualBox y Vagrant instalados, sigue los siguientes pasos:

- Muevete a la carpeta del proyecto

  `cd polymer-rails`

- Inicia la máquina virtual (tardará un poco la primera vez)

  `vagrant up`

- Reinicia la máquina virtual

  `vagrant reload`

- Conectate a la máquina virtual

  `vagrant ssh`

- Continua con las instrucciones de [«Instalación de gemas y modulos»](#instalacion-de-gemas-y-modulos)

#### Comandos útiles para el entorno virtual

- Para cerrar la conexión con la máquina virtual, usa la instrucción

  `exit`

- Para apagar la máquina virtual, usa

  `vagrant halt`.

- Para borar la maquina virtual, usa

  `vagrant destroy`

---
**Nota**: El código específico de éste repositorio es proporcionado bajo la licencia [CC0 Dedicación de Dominio Público][CC0].

[blog]: https://rojo.github.io/2018/05/25/web-components-con-rails/
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/deed.es
[localhost]: http://localhost:3000/
[vbox]: https://www.virtualbox.org/wiki/Downloads
[vagrant]: https://www.vagrantup.com/downloads.html
